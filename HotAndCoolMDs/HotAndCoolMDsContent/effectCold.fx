sampler s0;

float timer;

texture iceTex;  
sampler iceTex_sampler = sampler_state{Texture = iceTex;};

float4 PixelShaderFunction(float2 coords: TEXCOORD0) : COLOR0
{
    float4 color = tex2D(s0, coords);  
	if (color.a == 0)
	{
		return color;
	}

	float4 ice = tex2D(iceTex_sampler, coords);  
	float d = length(abs(coords-0.5f));
	return lerp(color,ice, d*d*0.5f);

}

technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
