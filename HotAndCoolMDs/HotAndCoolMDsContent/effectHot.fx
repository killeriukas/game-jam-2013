sampler s0;

float timer;

texture fireTex;  
sampler fireTex_sampler = sampler_state{Texture = fireTex;};

texture waveTex;  
sampler waveTex_sampler = sampler_state{Texture = waveTex;};

float4 PixelShaderFunction(float2 coords: TEXCOORD0) : COLOR0
{
	float2 aux = coords;
	aux.y += timer;
	float4 wave = tex2D(waveTex_sampler, aux);  
	coords += wave.rb*0.01f;

    float4 color = tex2D(s0, coords);  
	if (color.a == 0)
	{
		return color;
	}



	float4 fire = tex2D(fireTex_sampler, coords);  
	float d = length(abs(coords-0.5f));
	return lerp(color,fire, 0);

}

technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
