﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace HotAndCoolMDs
{
    public class Camera
    {

        internal Vector2 position;

        public Camera()
        {
            position = Vector2.Zero;
        }

        public Vector2 getPosition()
        {
            return position;
        }

        public Vector2 TransformObjectPosition(Vector2 objectPosition)
        {
            return objectPosition - position;
        }
    }
}
