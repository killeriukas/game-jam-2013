﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using GardeGames.Graphics2D;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace HotAndCoolMDs
{
    public class OneTeamScene : GardeGames.States.State
    {

        GameTeam m_TwoPlayers;
        private double m_TimeLeftSeconds;
        GardeGames.Core.ResourceManager m_ResourcesManager;
        GraphicsDevice m_graphicsDevice;
        Animation[] m_animation;
        Effect m_fxPlayer1, m_fxPlayer2, m_fxCold, m_fxHot;

        SpriteFont m_Font;

        SoundEffect beatHeart;
        Song backMusic;

        float actualBeatRate;
        float totalBeatRate;
        float maxBeatRate = 5000;
        float minBeatRate = 650;

        float vibrationTime;

        public OneTeamScene() : base("OneTeamScene")
        {
            //set timer for 600 seconds
            m_TimeLeftSeconds = 240000;

        }
        
        private void makeVibrate(GameTeam gameTeam)
        {
            if (gameTeam.holdingHeart())
            {
                PlayerIndex index = gameTeam.getHolderIndex();
                GamePad.SetVibration(index, 1.0f, 1.0f);
            }
        }

        private void decreaseVibrate(GameTeam gameTeam, float vib)
        {
            if (gameTeam.holdingHeart())
            {
                PlayerIndex index = gameTeam.getHolderIndex();
                GamePad.SetVibration(index, vib, vib);
            }
            else
            {
                GamePad.SetVibration(gameTeam.playerIndex1, 0, 0);
                GamePad.SetVibration(gameTeam.playerIndex2, 0, 0);
            }
        }


        protected void UpdateInput(long elapsedTime)
        {

        }

        protected void UpdateGameLogic(long elapsedTime)
        {
            //decrement life of patient
            m_TimeLeftSeconds -= elapsedTime;

            //check timer
            CheckTimeLeft();

            //if heart is in the place, finish the game
            if (m_TwoPlayers.isFinished())
            {
                setNextState("FinishScene");
            }

        }

        private void CheckTimeLeft()
        {
            if (m_TimeLeftSeconds < 0)
            {
                m_TimeLeftSeconds = 240000;
                setNextState("GameOverScene");
            }
        }

        public override void loadResources(GardeGames.Core.ResourceManager recursos, GraphicsDevice Graficos2D)
        {
            //saves pointer to resources
            m_ResourcesManager = recursos;
            m_graphicsDevice = Graficos2D;

            beatHeart = m_ResourcesManager.content.Load<SoundEffect>("Sounds/heartBeat");
            backMusic = m_ResourcesManager.content.Load<Song>("Sounds/winter");
        }

        public override void start(GardeGames.Input.InputManager managerTeclado)
        {
            //load the font
            m_Font = m_ResourcesManager.loadSpriteFont("Font/GameFont");
            m_animation = new Animation[6];
            m_animation[0] = new Animation(m_ResourcesManager.loadTexture2D("Images/walkAnim"), 4, 300);
            m_animation[1] = new Animation(m_ResourcesManager.loadTexture2D("Images/spellAnim"), 4, 300);

            m_animation[2] = new Animation(m_ResourcesManager.loadTexture2D("Images/walkAnim"), 4, 300);
            m_animation[3] = new Animation(m_ResourcesManager.loadTexture2D("Images/spellAnim"), 4, 300);

            for(int i = 4; i < m_animation.Length; ++i) {
                m_animation[i] = new Animation();
            }

            //set timer for 600 seconds
            m_TimeLeftSeconds = 240000;

            //m_animation[0].addFrame(m_ResourcesManager.loadTexture2D("Images/mega-man-sprite"), 100);
            m_animation[4].addFrame(m_ResourcesManager.loadTexture2D("Images/Heart"), 100);
            m_animation[5].addFrame(m_ResourcesManager.loadTexture2D("Images/Key"), 100);

            m_fxPlayer1 = m_ResourcesManager.content.Load<Effect>("inkNormal");
            m_fxPlayer2 = m_ResourcesManager.content.Load<Effect>("inkRed");
            m_fxCold = m_ResourcesManager.content.Load<Effect>("effectCold");
            m_fxCold.Parameters["iceTex"].SetValue(m_ResourcesManager.loadTexture2D("iceTex"));
            m_fxHot = m_ResourcesManager.content.Load<Effect>("effectHot");
            //m_fxHot.Parameters["fireTex"].SetValue(m_ResourcesManager.loadTexture2D("fireTex"));
            m_fxHot.Parameters["waveTex"].SetValue(m_ResourcesManager.loadTexture2D("wave"));
            m_TwoPlayers = new GameTeam(m_animation, m_fxPlayer1, m_fxPlayer2, m_fxCold, m_fxHot, PlayerIndex.One, PlayerIndex.Two, new Vector2(m_graphicsDevice.Viewport.Width, m_graphicsDevice.Viewport.Height), m_graphicsDevice);
            Level level = new Level(m_ResourcesManager.content, m_TwoPlayers);
            m_TwoPlayers.startLevel(level);

            MediaPlayer.Volume = 0.5f;
            MediaPlayer.Play(backMusic);

            actualBeatRate = totalBeatRate = maxBeatRate;
            beatHeart.Play();
        }

        public override void stop()
        {
            decreaseVibrate(m_TwoPlayers, 0);
            MediaPlayer.Stop();
        }

        public override void update(long elapsedTime)
        {

            UpdateInput(elapsedTime);

            UpdateGameLogic(elapsedTime);

            //update players input and game logic
            m_TwoPlayers.update(elapsedTime);

            actualBeatRate -= elapsedTime;
            if (actualBeatRate<=0)
            {
                if (totalBeatRate > minBeatRate)
                {
                    totalBeatRate *= 0.975f;
                }
                actualBeatRate = totalBeatRate;
                vibrationTime = minBeatRate;
                beatHeart.Play();
                makeVibrate(m_TwoPlayers);
            }

            if (vibrationTime > 0)
            {
                vibrationTime -= elapsedTime;
                if (vibrationTime < 0)
                {
                    vibrationTime=0;
                }
                decreaseVibrate(m_TwoPlayers, vibrationTime / minBeatRate);
            }

        }

        public override void draw(SpriteBatch s)
        {
            //call players draw method
            m_TwoPlayers.draw(s);

            //call begin once
            s.Begin();

            //draw player window
            s.Draw(m_TwoPlayers.getRenderTarget(), new Rectangle(0, (int)(m_graphicsDevice.Viewport.Height * 0.25f), m_graphicsDevice.Viewport.Width,
                                                        (int)(m_graphicsDevice.Viewport.Height * 0.5f)), Color.White);

            //draw GUI
            drawGUI(s);

            //call end once
            s.End();


        }


        private void drawGUI(SpriteBatch s)
        {
            //draw time limit for patient
            s.DrawString(m_Font, "Life of Patient: " + (int)(m_TimeLeftSeconds * 0.001), new Vector2(0, 0), Color.White);
        }

    }

}
