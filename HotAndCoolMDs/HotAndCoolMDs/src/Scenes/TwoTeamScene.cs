﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using GardeGames.Graphics2D;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace HotAndCoolMDs
{
    public class TwoTeamScene : GardeGames.States.State
    {
        //first team
        GameTeam m_AboveTwoPlayers;

        //second team
        GameTeam m_BottomTwoPlayers;

        //time limit for completion fo the game
        private double m_TimeLeftSeconds;

        GardeGames.Core.ResourceManager m_ResourcesManager;
        GraphicsDevice m_graphicsDevice;
        Animation[] m_animation;

        //shader files
        Effect m_fxPlayer1, m_fxPlayer2, m_fxCold, m_fxHot;

        SpriteFont m_Font;

        SoundEffect beatHeart;
        Song backMusic;

        float actualBeatRate;
        float totalBeatRate;
        float maxBeatRate = 5000;
        float minBeatRate = 650;

        float vibrationTime;

        public TwoTeamScene() : base("TwoTeamScene")
        {
            //set timer for 600 seconds
            m_TimeLeftSeconds = 240000;

        }

        private void makeVibrate(GameTeam gameTeam)
        {
            if (gameTeam.holdingHeart())
            {
                PlayerIndex index = gameTeam.getHolderIndex();
                GamePad.SetVibration(index, 1.0f, 1.0f);
            }
        }

        private void decreaseVibrate(GameTeam gameTeam, float vib)
        {
            if (gameTeam.holdingHeart())
            {
                PlayerIndex index = gameTeam.getHolderIndex();
                GamePad.SetVibration(index, vib, vib);
            }
            else
            {
                GamePad.SetVibration(gameTeam.playerIndex1, 0, 0);
                GamePad.SetVibration(gameTeam.playerIndex2, 0, 0);
            }
        }

        protected void UpdateInput(long elapsedTime)
        {

        }

        protected void UpdateGameLogic(long elapsedTime)
        {
            //decrement life of patient
            m_TimeLeftSeconds -= elapsedTime;

            //check timer
            CheckTimeLeft();

        }

        private void CheckTimeLeft()
        {
            if (m_TimeLeftSeconds < 0)
            {
                m_TimeLeftSeconds = 240000;
                setNextState("GameOverScene");
            }
        }
        

        public override void loadResources(GardeGames.Core.ResourceManager recursos, GraphicsDevice Graficos2D)
        {
            //saves pointer to resources
            m_ResourcesManager = recursos;
            m_graphicsDevice = Graficos2D;

            beatHeart = m_ResourcesManager.content.Load<SoundEffect>("Sounds/heartBeat");
            backMusic = m_ResourcesManager.content.Load<Song>("Sounds/winter");
        }

        public override void start(GardeGames.Input.InputManager managerTeclado)
        {
            //load the font
            m_Font = m_ResourcesManager.loadSpriteFont("Font/GameFont");
            m_animation = new Animation[6];
            m_animation[0] = new Animation(m_ResourcesManager.loadTexture2D("Images/walkAnim"), 4, 300);
            m_animation[1] = new Animation(m_ResourcesManager.loadTexture2D("Images/spellAnim"), 4, 300);

            m_animation[2] = new Animation(m_ResourcesManager.loadTexture2D("Images/walkAnim"), 4, 300);
            m_animation[3] = new Animation(m_ResourcesManager.loadTexture2D("Images/spellAnim"), 4, 300);

            for (int i = 4; i < m_animation.Length; ++i)
            {
                m_animation[i] = new Animation();
            }

            //set timer for 600 seconds
            m_TimeLeftSeconds = 240000;

            //m_animation[0].addFrame(m_ResourcesManager.loadTexture2D("Images/mega-man-sprite"), 100);
            m_animation[4].addFrame(m_ResourcesManager.loadTexture2D("Images/Heart"), 100);
            m_animation[5].addFrame(m_ResourcesManager.loadTexture2D("Images/Key"), 100);
            m_fxPlayer1 = m_ResourcesManager.content.Load<Effect>("inkNormal");
            m_fxPlayer2 = m_ResourcesManager.content.Load<Effect>("inkRed");
            m_fxCold = m_ResourcesManager.content.Load<Effect>("effectCold");
            m_fxCold.Parameters["iceTex"].SetValue(m_ResourcesManager.loadTexture2D("iceTex"));
            m_fxHot = m_ResourcesManager.content.Load<Effect>("effectHot");
            //m_fxHot.Parameters["fireTex"].SetValue(m_ResourcesManager.loadTexture2D("fireTex"));
            m_fxHot.Parameters["waveTex"].SetValue(m_ResourcesManager.loadTexture2D("wave"));
            m_AboveTwoPlayers = new GameTeam(m_animation, m_fxPlayer1, m_fxPlayer2, m_fxCold, m_fxHot, PlayerIndex.One, PlayerIndex.Two, new Vector2(m_graphicsDevice.Viewport.Width, m_graphicsDevice.Viewport.Height), m_graphicsDevice);

            m_animation[0] = new Animation(m_ResourcesManager.loadTexture2D("Images/walkAnim"), 4, 300);
            m_animation[1] = new Animation(m_ResourcesManager.loadTexture2D("Images/spellAnim"), 4, 300);

            m_animation[2] = new Animation(m_ResourcesManager.loadTexture2D("Images/walkAnim"), 4, 300);
            m_animation[3] = new Animation(m_ResourcesManager.loadTexture2D("Images/spellAnim"), 4, 300);

            m_BottomTwoPlayers = new GameTeam(m_animation, m_fxPlayer1, m_fxPlayer2, m_fxCold, m_fxHot, PlayerIndex.Three, PlayerIndex.Four, new Vector2(m_graphicsDevice.Viewport.Width, m_graphicsDevice.Viewport.Height), m_graphicsDevice);
            
            Level level1 = new Level(m_ResourcesManager.content, m_AboveTwoPlayers);
            m_AboveTwoPlayers.startLevel(level1);

            Level level2 = new Level(m_ResourcesManager.content, m_BottomTwoPlayers);
            m_BottomTwoPlayers.startLevel(level2);

            MediaPlayer.Volume = 0.5f;
            MediaPlayer.Play(backMusic);

            actualBeatRate = totalBeatRate = maxBeatRate;
            beatHeart.Play();
        }

        public override void stop()
        {
            decreaseVibrate(m_AboveTwoPlayers, 0);
            decreaseVibrate(m_BottomTwoPlayers, 0);
            MediaPlayer.Stop();
        }

        public override void update(long elapsedTime)
        {

            UpdateInput(elapsedTime);

            UpdateGameLogic(elapsedTime);

            //update players input and game logic
            m_AboveTwoPlayers.update(elapsedTime);

            //update players input and game logic
            m_BottomTwoPlayers.update(elapsedTime);

            if (m_AboveTwoPlayers.isFinished())
            {
                setNextState("FinishSceneTeam1");
                return;
            }

            if (m_BottomTwoPlayers.isFinished())
            {
                setNextState("FinishSceneTeam2");
                return;
            }

            actualBeatRate -= elapsedTime;
            if (actualBeatRate <= 0)
            {
                if (totalBeatRate > minBeatRate)
                {
                    totalBeatRate *= 0.975f;
                }
                actualBeatRate = totalBeatRate;
                vibrationTime = minBeatRate;
                beatHeart.Play();
                makeVibrate(m_AboveTwoPlayers);
                makeVibrate(m_BottomTwoPlayers);
            }

            if (vibrationTime > 0)
            {
                vibrationTime -= elapsedTime;
                if (vibrationTime < 0)
                {
                    vibrationTime = 0;
                }
                decreaseVibrate(m_AboveTwoPlayers, vibrationTime / minBeatRate);
                decreaseVibrate(m_BottomTwoPlayers, vibrationTime / minBeatRate);
            }


        }

        public override void draw(SpriteBatch s)
        {
            //call players draw method
            m_AboveTwoPlayers.draw(s);

            //call second team players draw method
            m_BottomTwoPlayers.draw(s);

            //call begin once
            s.Begin();

            //draw above player window
            s.Draw(m_AboveTwoPlayers.getRenderTarget(), new Rectangle(0, (int)(m_graphicsDevice.Viewport.Height * 0.0f), m_graphicsDevice.Viewport.Width,
                                                        (int)(m_graphicsDevice.Viewport.Height * 0.5f)), Color.White);
            //draw bottom player window
            s.Draw(m_BottomTwoPlayers.getRenderTarget(), new Rectangle(0, (int)(m_graphicsDevice.Viewport.Height * 0.5f), m_graphicsDevice.Viewport.Width,
                                                        (int)(m_graphicsDevice.Viewport.Height * 0.5f)), Color.White);

            //draw GUI
            drawGUI(s);


            //call end once
            s.End();


        }

        private void drawGUI(SpriteBatch s)
        {
            //draw time limit for patient
            s.DrawString(m_Font, "Life of Patient: " + (int)(m_TimeLeftSeconds * 0.001), new Vector2(50, 50), Color.Black);
        }


    }
}
