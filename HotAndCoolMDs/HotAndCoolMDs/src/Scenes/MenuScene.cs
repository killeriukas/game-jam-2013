﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using GardeGames.States;

namespace HotAndCoolMDs
{
    class MenuScene : GardeGames.States.State
    {
        SpriteFont m_FontTip;
        SpriteFont m_Font;
        GardeGames.Core.ResourceManager m_ResourcesManager;
        private PlayerChoice m_playerChoice;

        int m_ScreenWidth, m_ScreenHeight;

        Texture2D mBackground;

        private enum PlayerChoice
        {
            DEFAULT = 0,
            TWO_PLAYER = 2,
            FOUR_PLAYER = 4
        }

        private enum MenuChoice
        {
            EXIT_GAME = 0,
            FOUR_P_PLAY = 1,
            TWO_P_PLAY = 2
        }

        private MenuChoice m_currentMenuChoice;
        private Color m_twoPlayerColor, m_fourPlayerColor, m_exitGameColor;

        private long m_waitTime;

        private bool m_ShowTip;

        string[] m_Sentence;

        public MenuScene() : base("MenuScene")
        {
            m_playerChoice = PlayerChoice.DEFAULT;
            m_currentMenuChoice = MenuChoice.TWO_P_PLAY;
            ResetKeysColors();
            m_twoPlayerColor = Color.Black;
            m_waitTime = 150;
        }



        public override void loadResources(GardeGames.Core.ResourceManager recursos, Microsoft.Xna.Framework.Graphics.GraphicsDevice Graficos2D)
        {
            m_Font = recursos.loadSpriteFont("Font/MenuFont");
            m_FontTip = recursos.loadSpriteFont("Font/TipFont");
            m_ResourcesManager = recursos;
            m_ScreenWidth = Graficos2D.Viewport.Width;
            m_ScreenHeight = Graficos2D.Viewport.Height;

            mBackground = recursos.loadTexture2D("Images/startScreen");
        }

        private void ResetKeysColors()
        {
            m_twoPlayerColor = m_fourPlayerColor = m_exitGameColor = Color.White;
            m_ShowTip = false;
        }

        public override void start(GardeGames.Input.InputManager managerTeclado)
        {
            m_Sentence = new string[4];

            //calculate all the strings length and center it
            m_Sentence[0] = "Two Players";
            m_Sentence[1] = "Four Players";
            m_Sentence[2] = "Exit Game";
            m_Sentence[3] = "Tip: You can't play four player match, unless you have 4 controllers connected!";

            //LOAD MAIN MENU EVERYTHING
           //m_ResourcesManager
        }

        public override void stop()
        {
           //UNLOAD SOMETHING
        }

        private void MoveDown()
        {
            m_currentMenuChoice--;
            if(m_currentMenuChoice < 0) {
                m_currentMenuChoice = MenuChoice.TWO_P_PLAY;
            }
        }

        private void MoveUp()
        {

            m_currentMenuChoice++;
            if (m_currentMenuChoice > MenuChoice.TWO_P_PLAY)
            {
                m_currentMenuChoice = MenuChoice.EXIT_GAME;
            }
        }

        public override void update(long elapsedTime)
        {
            //update input
            UpdateInput(elapsedTime);

            //update game logics
            UpdateGameLogic(elapsedTime);
        }

        private void CheckMovement(PlayerIndex index,long elapsedTime)
        {
            float posY = GamePad.GetState(index).ThumbSticks.Left.Y;

            //Check for movement
            if (posY != 0)
            {
                ResetKeysColors();

                m_waitTime -= elapsedTime;
                if (m_waitTime < 0)
                {
                    if (posY > 0)
                    {
                        MoveUp();
                    }
                    if (posY < 0)
                    {
                        MoveDown();
                    }
                    m_waitTime = 150;
                }
                switch (m_currentMenuChoice)
                {
                    case MenuChoice.EXIT_GAME: m_exitGameColor = Color.Black; break;
                    case MenuChoice.TWO_P_PLAY: m_twoPlayerColor = Color.Black; break;
                    case MenuChoice.FOUR_P_PLAY: m_fourPlayerColor = Color.Black; break;
                }
            }
            if (GamePad.GetState(index).IsButtonDown(Buttons.A) || Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                switch (m_currentMenuChoice)
                {
                    case MenuChoice.EXIT_GAME: setNextState(StateManager.END_GAME); break;
                    case MenuChoice.TWO_P_PLAY: setNextState("OneTeamScene"); break;
                    //case MenuChoice.FOUR_P_PLAY: if (GamePad.GetState(PlayerIndex.Four).IsConnected) { setNextState("TwoTeamScene"); } else { m_ShowTip = true; } break;
                    case MenuChoice.FOUR_P_PLAY: setNextState("TwoTeamScene"); break;
                }
            }
        }

        private void UpdateInput(long elapsedTime)
        {

            //first player controller
            CheckMovement(PlayerIndex.One, elapsedTime);

            ////second player controller
            //CheckMovement(PlayerIndex.Two);

            ////third player controller
            //CheckMovement(PlayerIndex.Three);

            ////fourth player controller
            //CheckMovement(PlayerIndex.Four);

            //if(Keyboard.GetState().IsKeyDown(Keys.G)) {
            //    m_playerChoice = PlayerChoice.TWO_PLAYER;
            //}
           
            //if(Keyboard.GetState().IsKeyDown(Keys.H)) {
            //    m_playerChoice = PlayerChoice.FOUR_PLAYER;
            //}



            //if() {

            //} else {

            //}

        }

        private void UpdateGameLogic(long elapsedTime)
        {
            if (m_playerChoice == PlayerChoice.TWO_PLAYER)
            {
                setNextState("OneTeamScene");
            }

            if (m_playerChoice == PlayerChoice.FOUR_PLAYER)
            {
                setNextState("TwoTeamScene");
            } 

            //IF 4 PLAYERS
            //if (m_playerChoice == PlayerChoice.FOUR_PLAYER)
            //{
            //    setNextState("TwoTeamScene");
            //}
        }

        public override void draw(Microsoft.Xna.Framework.Graphics.SpriteBatch s)
        {
            s.Begin();

            //draw background
            s.Draw(mBackground, new Rectangle(0, 0, m_ScreenWidth, m_ScreenHeight), Color.White);

            //draw shitty menu
            s.DrawString(m_Font, m_Sentence[0], new Vector2((m_ScreenWidth - m_Font.MeasureString(m_Sentence[0]).X) * 0.5f, m_ScreenHeight * 0.5f - 100), m_twoPlayerColor);
            s.DrawString(m_Font, m_Sentence[1], new Vector2((m_ScreenWidth - m_Font.MeasureString(m_Sentence[1]).X) * 0.5f, m_ScreenHeight * 0.5f - 50), m_fourPlayerColor);
            s.DrawString(m_Font, m_Sentence[2], new Vector2((m_ScreenWidth - m_Font.MeasureString(m_Sentence[2]).X) * 0.5f, m_ScreenHeight * 0.5f), m_exitGameColor);

            if (m_ShowTip) {
                s.DrawString(m_FontTip, m_Sentence[3], new Vector2((m_ScreenWidth - m_FontTip.MeasureString(m_Sentence[3]).X) * 0.5f, m_ScreenHeight - 100), Color.BlueViolet);
            }

            s.End();
        }
    }
}
