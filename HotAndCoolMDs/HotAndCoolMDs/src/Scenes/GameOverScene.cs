﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace HotAndCoolMDs
{
    class GameOverScene : GardeGames.States.State
    {
        
        SpriteFont m_Font;
        string m_message = "You have lost the patient!";
        float m_screenW, m_screenH;
        private long deathScreen;

        public GameOverScene()
            : base("GameOverScene")
        {

        }


        public override void loadResources(GardeGames.Core.ResourceManager recursos, Microsoft.Xna.Framework.Graphics.GraphicsDevice Graficos2D)
        {
            m_screenW = Graficos2D.Viewport.Width * 0.5f;
            m_screenH = Graficos2D.Viewport.Height * 0.5f;
            m_Font = recursos.loadSpriteFont("Font/FinishFont");
        }

        public override void start(GardeGames.Input.InputManager managerTeclado)
        {
            //3 seconds
            deathScreen = 3000;
        }

        public override void stop()
        {
            //NOTHING
        }

        public override void update(long elapsedTime)
        {
            deathScreen -= elapsedTime;
            if (deathScreen < 0)
            {
                deathScreen = 3000;
                setNextState("MenuScene");
            }
        }

        public override void draw(Microsoft.Xna.Framework.Graphics.SpriteBatch s)
        {
            s.Begin();
            s.DrawString(m_Font, m_message, new Vector2(m_screenW - m_Font.MeasureString(m_message).X * 0.5f, m_screenH - 50), Color.White);
            s.End();
        }
    }
}
