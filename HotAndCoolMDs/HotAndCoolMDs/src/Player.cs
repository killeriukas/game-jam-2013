﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GardeGames.Graphics2D;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace HotAndCoolMDs
{
    public class Player
    {
        public enum States
        {
            Walk,
            SpellCast,
            Stop,
        };

        Animation[] animation;
        Vector2 position;

        Effect inkEffect;

        Vector2 speed;

        States actualState;

        private bool canJump = false;


        public bool facesRight;
        public bool grabbing;

        public enum Ability
        {
            Heat,
            Cool,
        };
        Ability ability;

        public Player(Animation[] animation, Effect inkEffect, Vector2 position, Ability ability)
        {
            this.animation = animation;
            this.position = position;
            this.inkEffect = inkEffect;
            this.ability = ability;

            //speed = new Vector2(0.25f, 0);
            speed = new Vector2(0.6f, 0);

            facesRight = true;
            grabbing = false;
        }

        public void setState(States state)
        {
            actualState = state;
        }

        public void setCanJump(bool canJump)
        {
            this.canJump = canJump;
        }

        public bool getCanJump()
        {
            return canJump;
        }

        public void jump(float force)
        {
            if (canJump)
            {
                speed.Y = -force;
            }
        }

        public void setPosition(Vector2 position)
        {
            this.position = position;
        }

        public Vector2 getPosition()
        {
            return position;
        }

        public Vector2 getSize()
        {
            return new Vector2(64, 128);
        }

        public Vector2 getSpeed()
        {
            return speed;
        }

        public void setVertSpeed(float speed)
        {
            this.speed.Y = speed;
        }

        public void update(long elapsedTime)
        {
            switch (actualState)
            {
                case States.SpellCast:
                case States.Walk:
                    animation[(int)actualState].update(elapsedTime);
                    break;
            }
        }

        public void applyInkEffect()
        {
            if (inkEffect != null)
            {
                inkEffect.CurrentTechnique.Passes[0].Apply();
            }
        }

        public Texture2D getFrameTexture()
        {
            return animation[(int)actualState].getTexture2D();
        }

        public KeyValuePair<Texture2D, Rectangle> getSpriteSheet()
        {
            return animation[(int)actualState].getSpriteSheetFrame();
        }

        public Ability changeWeather()
        {
            return ability;
        }

    }
}
