﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GardeGames.Graphics2D;
using Microsoft.Xna.Framework.Graphics;

namespace HotAndCoolMDs.src
{
    public class Transportable
    {
        public enum Type
        {
            Heart,
            Key
        };

        Type type;
        Animation animation;
        Vector2 position;
        
        private int m_Object_ID;

        bool attached;
        public Player grabber;

        public Transportable(Type type, Animation animation, Vector2 position, int oID)
        {
            this.type = type;
            this.animation = animation;
            this.position = position;
            attached = false;
            grabber = null;
            m_Object_ID = oID;
        }

        public int GetKeyID()
        {
            if(type == Type.Key) {
                return m_Object_ID;
            }
            return -1;

        }

        public bool isAttached()
        {
            return attached;
        }

        public void grab(Player grabber)
        {
            if (!attached)
            {
                attached = true;
                this.grabber = grabber;
            }
        }

        public void drop()
        {
            if (attached)
            {
                attached = false;
                grabber = null;
            }
        }

        public void update(long elapsedTIme)
        {
            animation.update(elapsedTIme);
            if (attached)
            {
                position = grabber.getPosition();
            }
        }

        public void setPosition(Vector2 position)
        {
            if (!attached)
            {
                this.position = position;
            }
        }

        public Texture2D getFrameTexture()
        {
            return animation.getTexture2D();
        }

        public Vector2 getPosition()
        {
            return position;
        }
    }
}
