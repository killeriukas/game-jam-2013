using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using GardeGames.Core;
using HotAndCoolMDs;

namespace HotAndCoolMDs
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : GardeGames.Core.MainGame
    {
        public Game1()
            : base()
        {
            //initialize menu scene
            stateManager.addState(new MenuScene());

            //initialize two player game
            stateManager.addState(new OneTeamScene());

            //initialize four player game
            stateManager.addState(new TwoTeamScene());

            //initialize finish screen
            stateManager.addState(new FinishScene());
            stateManager.addState(new FinishSceneTeam1());
            stateManager.addState(new FinishSceneTeam2());

            //initialize game over screen
            stateManager.addState(new GameOverScene());

        }

        protected override void LoadContent()
        {
            base.LoadContent();
            stateManager.setState("MenuScene");
        }
    }
}
