﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace HotAndCoolMDs
{
    public static class MapCreator
    {
        static Random rand = new Random();
        static int topLimit = 0;
        static int bottomLimit = 0;
        static int worldWidth = 0;
        static int worldHeight = 0;

        static int numberOfDoors = 0;
        const int MAXDOORS = 5;

        static int numberOfPools = 0;
        const int MAXPOOLS = 15;

        public static int[,] createPath(int[,] map, int width, int height, int topLim, int bottomLim)
        {
            int[,] newMap = new int[width, height];
            topLimit = topLim;
            bottomLimit = bottomLim;
            worldWidth = width;
            worldHeight = height;


            for (int i = 0; i < width; i++) {
                for (int c = 0; c < height; c++) {
                    newMap[i, c] = map[i, c];
                }
            }
            
            Vector2 currentRoom = new Vector2(0, height - 3);
            newMap[(int)currentRoom.X, (int)height - 3] = (int)Tile.Type.GRASS;
            newMap[(int)currentRoom.X, (int)height - 2] = (int)Tile.Type.FILL;
            newMap[(int)currentRoom.X, (int)height - 1] = (int)Tile.Type.FILL;
            
            for (int i = 0; i < 6; i++ ) 
            {
                currentRoom.X++;
                newMap[(int)currentRoom.X, (int)height - 3] = (int)Tile.Type.GRASS;
                newMap[(int)currentRoom.X, (int)height - 2] = (int)Tile.Type.FILL;
                newMap[(int)currentRoom.X, (int)height - 1] = (int)Tile.Type.FILL;
            }

            
            Vector2 lastRoom = new Vector2(currentRoom.X, currentRoom.Y);
            int counter = 0;
            for (int i = 0; i < width-7; i++)
            {
                counter++;
                if (i % 2 == 0)
                {
                    currentRoom = getNextTile(currentRoom);
                }
                else 
                {
                    currentRoom.X++;
                }

                newMap[(int)currentRoom.X, (int)currentRoom.Y] = (int)Tile.Type.GRASS;
                
                for (int d = 1; d < height - currentRoom.Y; d++)
                {
                    newMap[(int)currentRoom.X, (int)currentRoom.Y + d] = (int)Tile.Type.FILL;
                }

                if (currentRoom.Y < height - 5 && numberOfPools < MAXPOOLS)
                {
                    if (rand.Next(11) == 3 && counter > 10)
                    {
                        newMap = createPool(currentRoom, newMap);
                        currentRoom.X += 11;
                        i += 11;
                        counter = 0;
                    }
                }

                else if (numberOfDoors < MAXDOORS)
                {
                    if (rand.Next(20) == 3 && counter > 10 && currentRoom.X < 520)
                    {
                        newMap = createDoor(currentRoom, newMap);
                        currentRoom.X +=2;
                        i +=2;
                        numberOfDoors++;
                        counter = 0;
                    }
                }

                lastRoom = new Vector2(currentRoom.X, currentRoom.Y);
            }

            Console.WriteLine(numberOfPools + " / " + MAXPOOLS);
            Console.WriteLine(numberOfDoors + " / " + MAXDOORS);
            return newMap;
        }

        public static int[,] createDoor(Vector2 currentTile, int[,] newMap)
        {
            currentTile.X++;

            newMap[(int)currentTile.X, (int)currentTile.Y - 2] = (int)Tile.Type.DOOR;

            for (int i = worldHeight-1; i >= currentTile.Y; i--)
            {

                if (i == currentTile.Y)
                {
                    newMap[(int)currentTile.X, (int)i] = (int)Tile.Type.GRASS;
                }
                else
                {
                    newMap[(int)currentTile.X, (int)i] = (int)Tile.Type.FILL;
                }
            }

            for (int i = 0; i < currentTile.Y - 2; i++)
            {
                newMap[(int)currentTile.X, (int)i] = (int)Tile.Type.FILL;
            }

            currentTile.X++;

            for (int i = worldHeight - 1; i >= currentTile.Y; i--)
            {

                if (i == currentTile.Y)
                {
                    newMap[(int)currentTile.X, (int)i] = (int)Tile.Type.GRASS;
                }
                else
                {
                    newMap[(int)currentTile.X, (int)i] = (int)Tile.Type.FILL;
                }
            }


            return newMap;

        }

        public static int[,] createPool(Vector2 currentTile, int[,] newMap)
        {
            currentTile.X++;
            
            int length = 11;
            
            bool freezePond = true;
            
            if (rand.Next(2) == 0)
            {
                freezePond = false;
            }


            if (!freezePond)
            {
                for (int d = (int)currentTile.Y; d > 0; d--)
                {
                    newMap[(int)currentTile.X + 4, (int)(currentTile.Y - d)] = (int)Tile.Type.FILL;
                }

            }

            for (int i = 0; i < length - 1; i++)
            {

                if (freezePond)
                {
                    for (int d = 0; d < i + 2; d+=2)
                    {
                        newMap[(int)currentTile.X + i, (int)(currentTile.Y + Math.Ceiling(d*0.5f))] = (int)Tile.Type.WATER;
                    }
                    if (i == 0)
                    {
                        newMap[(int)currentTile.X + i, (int)currentTile.Y + 1] = (int)Tile.Type.FILL;
                        newMap[(int)currentTile.X + i, (int)currentTile.Y + 2] = (int)Tile.Type.FILL;
                    }
                    else if (i == 1 || i == 2)
                    {
                        newMap[(int)currentTile.X + i, (int)currentTile.Y + 2] = (int)Tile.Type.FILL;
                    }
                }
                else
                {
                    newMap[(int)currentTile.X + i, (int)currentTile.Y + 0] = (int)Tile.Type.WATER;
                    newMap[(int)currentTile.X + i, (int)currentTile.Y + 1] = (int)Tile.Type.WATER;
                    newMap[(int)currentTile.X + i, (int)currentTile.Y + 2] = (int)Tile.Type.FILL;
                }

                for (int d = 1; d < worldHeight - (currentTile.Y + 2); d++)
                {
                    newMap[(int)currentTile.X + i, (int)(currentTile.Y + 2) + d] = (int)Tile.Type.FILL;
                }


            }

            newMap[(int)currentTile.X + 10, (int)currentTile.Y] = (int)Tile.Type.GRASS;

            for (int d = 1; d < worldHeight - (currentTile.Y); d++)
            {
                newMap[(int)currentTile.X + 10, (int)currentTile.Y + d] = (int)Tile.Type.FILL;
            }

            numberOfPools++;
            

            return newMap;
        }

        static int lastRoll = 1;
        public static Vector2 getNextTile(Vector2 currentTile)
        {
            Vector2 nextTile = new Vector2(currentTile.X + 1, currentTile.Y);

            if (currentTile.X < worldWidth - 50)
            {
                int roll = rand.Next(4);

                if (roll == 0)
                {
                    roll = lastRoll;
                }

                if (roll == 1)
                {
                    if (currentTile.Y > topLimit)
                    {
                        nextTile.Y--;
                    }
                }
                else if (roll == 2)
                {
                    if (currentTile.Y < bottomLimit)
                    {
                        nextTile.Y++;
                    }
                }

                lastRoll = roll;
            }
            else
            {
                if (currentTile.Y < bottomLimit)
                {
                    nextTile.Y++;
                }
            }


            return nextTile;
        }

    }
}
