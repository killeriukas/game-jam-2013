﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace HotAndCoolMDs
{
    public class Level
    {
        internal LevelSection currentSection;

        public static int distanceBetweenSections = 8192;
        private GameTeam gameTeam;
        //Loads one of each different tile type in an array that can be accessed with Tile.Type.(-whichever tile). i.e. Tile.Type.GRASS.
        internal Tile[] tileList = new Tile[(int)Tile.Type.COUNT];

        public Level(ContentManager Content, GameTeam gameTeam)
        {
            this.gameTeam = gameTeam;
            tileList[(int)Tile.Type.AIR] = new AirTile();
            tileList[(int)Tile.Type.GRASS] = new GrassTile(Content.Load<Texture2D>("Level//Tiles//woodGround"), new Vector2(64, 64));
            tileList[(int)Tile.Type.GRASSEND] = new GrassTile(Content.Load<Texture2D>("Level//Tiles//woodEnd"), new Vector2(64, 64));

            tileList[(int)Tile.Type.SAND] = new SandTile(Content.Load<Texture2D>("Level//Tiles//desertGround"), new Vector2(64, 64));
            tileList[(int)Tile.Type.SANDEND] = new SandTile(Content.Load<Texture2D>("Level//Tiles//desertEnd"), new Vector2(64, 64));

            tileList[(int)Tile.Type.MOUNTAIN] = new MountainTile(Content.Load<Texture2D>("Level//Tiles//mountainGround"), new Vector2(64, 64));
            tileList[(int)Tile.Type.MOUNTAINEND] = new MountainTile(Content.Load<Texture2D>("Level//Tiles//mountainEnd"), new Vector2(64, 64));

            tileList[(int)Tile.Type.ROAD] = new RoadTile(Content.Load<Texture2D>("Level//Tiles//nightGround"), new Vector2(64, 64));
            tileList[(int)Tile.Type.ROADEND] = new RoadTile(Content.Load<Texture2D>("Level//Tiles//nightEnd"), new Vector2(64, 64));

            tileList[(int)Tile.Type.CITY] = new RoadTile(Content.Load<Texture2D>("Level//Tiles//cityGround"), new Vector2(64, 64));

            tileList[(int)Tile.Type.WATER] = new WaterTile(Content.Load<Texture2D>("Level//Tiles//waterIce"), new Vector2(64, 64), gameTeam);
            tileList[(int)Tile.Type.DOOR] = new DoorTile(Content.Load<Texture2D>("Images/door"), new Vector2(64, 128));
            tileList[(int)Tile.Type.FILL] = new DoorTile(Content.Load<Texture2D>("Level//Tiles//fill"), new Vector2(64, 128));

            currentSection = new SectionOfEverything(this, new GrassTile(Content.Load<Texture2D>("Level//Tiles//desertGround"), new Vector2(64, 64)), Content);
        }

        public Vector2 getTileCollision(Vector2 playerPosition, Vector2 playerSize, float newX, float newY, bool calledForY)
        {

            Vector2 collisionPoint = new Vector2(-1, -1);
            float fromX = Math.Min(playerPosition.X, newX);
            float fromY = Math.Min(playerPosition.Y, newY);
            float toX = Math.Max(playerPosition.X, newX);
            float toY = Math.Max(playerPosition.Y, newY);

            // get the tile locations
            int fromTileX = (int)((fromX - playerSize.X * 0.5f) / 64);
            int fromTileY = (int)((fromY - playerSize.Y * 0.5f) / 64);
            int toTileX = (int)((toX + playerSize.X * 0.5f - 1) / 64);
            int toTileY = (int)((toY + playerSize.Y * 0.5f - 1) / 64);

            // check each tile for a collision
            Vector2 displacement = new Vector2(currentSection.width, currentSection.height);

            for (int x = fromTileX; x <= toTileX; x++)
            {
                for (int y = fromTileY; y <= toTileY; y++)
                {
                    if (x < 0 ||
                        y < 0)
                    {
                        collisionPoint.X = x;
                        collisionPoint.Y = y;
                        return collisionPoint;
                    }
                    int type = (int)Tile.Type.AIR;
                    /*if (x - (int)displacement.X < -1)
                    {
                        type = formerSection.layout[x, y];
                    }
                    else*/
                    {
                        type = currentSection.layout[x % (int)displacement.X, y];
                    }

                    if (type != (int)Tile.Type.AIR)
                    {
                        if (gameTeam.actualWeather == GameTeam.Weather.Hot && type == (int)Tile.Type.WATER)
                        {
                            continue;
                        }

                        if (calledForY && gameTeam.actualWeather == GameTeam.Weather.Warm && type == (int)Tile.Type.WATER)
                        {
                            continue;
                        }
                        // collision found, return the tile
                        collisionPoint.X = x;
                        collisionPoint.Y = y;
                        return collisionPoint;
                    }
                }
            }

            // no collision found
            return collisionPoint;
        }

        public Vector2 CheckCollision(Vector2 playerPosition, Vector2 playerSize)
        {

            if (playerPosition.X < currentSection.position.X + playerSize.X)
            {
                //These will be changed later.
                //formerSection.CheckCollision(playerPosition, playerSize);
            }
            else if (playerPosition.X < currentSection.position.X + currentSection.width * Tile.STANDARD_SIZE - playerSize.X)
            {
                //nextSection.CheckCollision(playerPosition, playerSize);
            }

            return currentSection.CheckCollision(playerPosition, playerSize);
        }


        public void setCurrentSection(LevelSection currentSection)
        {
            this.currentSection = currentSection;
        }

        public void Update(long elapsedTime)
        {
            tileList[(int)Tile.Type.WATER].Update(elapsedTime);

        }

        public void Draw(Camera camera, SpriteBatch spriteBatch)
        {
            try
            {
                currentSection.Draw(camera, spriteBatch);
            }
            catch (NullReferenceException ex)
            {
            }
        }
    }
}
