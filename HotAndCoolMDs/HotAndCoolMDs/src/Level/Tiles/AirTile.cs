﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace HotAndCoolMDs
{
    public class AirTile : Tile
    {
        public AirTile() :
            base()
        {
        }

        public override bool CheckCollisionWithPlayer(Vector2 tilePosition, Vector2 newPlayerPosition, Vector2 playerSize)
        {
            //Do nothing. Not like it's being called anyway though. Shouldn't be!
            return false;
        }

        public override void Draw(Vector2 position, SpriteBatch spriteBatch)
        {
            //Do nothing
        }

    }
}
