﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace HotAndCoolMDs
{

    public class WaterTile : Tile
    {
        Rectangle? drawRectangle;
        GameTeam gameTeam;

        GameTeam.Weather weather;

        bool changingToSteam = false;
        bool changingFromSteam = false;
        bool changingToIce = false;
        bool changingFromIce = false;

        long changeCounter = 0;
        int icePercentage = 0;
        Rectangle? iceRect = new Rectangle?(new Rectangle(0, 128, 64, 64));


        public WaterTile(Texture2D texture, Vector2 size, GameTeam gameTeam) 
            : base(texture, size) 
        {
            this.gameTeam = gameTeam;
            weather = gameTeam.actualWeather;

            type = Type.WATER;
            drawRectangle = new Rectangle?(new Rectangle(0, 64, 64, 64));
        }


        public override bool CheckCollisionWithPlayer(Vector2 tilePosition, Vector2 newPlayerPosition, Vector2 playerSize)
        {
            if (weather == GameTeam.Weather.Hot)
            {
                //Ignore.
                return false;
            }

            if (weather == GameTeam.Weather.Warm)
            {
                if (tilePosition.X < newPlayerPosition.X && tilePosition.X + size.X > newPlayerPosition.X)
                {
                    if (tilePosition.Y < newPlayerPosition.Y && tilePosition.Y + size.Y > newPlayerPosition.Y)
                    {
                        //Collides with top of player.
                        return true;

                    }
                    else if (tilePosition.Y < newPlayerPosition.Y + playerSize.Y && tilePosition.Y + size.Y > newPlayerPosition.Y + playerSize.Y)
                    {
                        //Collides with bottom of player.
                        return true;
                    }
                }
                else if (tilePosition.X < newPlayerPosition.X + playerSize.X && tilePosition.X + size.X > newPlayerPosition.X + playerSize.X)
                {
                    if (tilePosition.Y < newPlayerPosition.Y && tilePosition.Y + size.Y > newPlayerPosition.Y)
                    {
                        //Collides with top of player.
                        return true;
                    }
                    else if (tilePosition.Y < newPlayerPosition.Y + playerSize.Y && tilePosition.Y + size.Y > newPlayerPosition.Y + playerSize.Y)
                    {
                        //Collides with bottom of player.
                        return true;
                    }
                }
            }

            if (weather == GameTeam.Weather.Cold)
            {

                //Check collision
                if ((tilePosition.Y > newPlayerPosition.Y && tilePosition.Y + size.Y < newPlayerPosition.Y + playerSize.Y))
                {
                    if (tilePosition.X < newPlayerPosition.X && tilePosition.X + size.X > newPlayerPosition.X)
                    {
                        //Left of player
                        return true;

                    }
                    else if (tilePosition.X < newPlayerPosition.X + playerSize.X && tilePosition.X + size.X > newPlayerPosition.X + playerSize.X)
                    {
                        //Right
                        return true;
                    }
                }

                if (tilePosition.X < newPlayerPosition.X && tilePosition.X + size.X > newPlayerPosition.X)
                {
                    if (tilePosition.Y < newPlayerPosition.Y && tilePosition.Y + size.Y > newPlayerPosition.Y)
                    {
                        //Collides with top of player.
                        return true;

                    }
                    else if (tilePosition.Y < newPlayerPosition.Y + playerSize.Y && tilePosition.Y + size.Y > newPlayerPosition.Y + playerSize.Y)
                    {
                        //Collides with bottom of player.
                        return true;
                    }
                }
                else if (tilePosition.X < newPlayerPosition.X + playerSize.X && tilePosition.X + size.X > newPlayerPosition.X + playerSize.X)
                {
                    if (tilePosition.Y < newPlayerPosition.Y && tilePosition.Y + size.Y > newPlayerPosition.Y)
                    {
                        //Collides with top of player.
                        return true;
                    }
                    else if (tilePosition.Y < newPlayerPosition.Y + playerSize.Y && tilePosition.Y + size.Y > newPlayerPosition.Y + playerSize.Y)
                    {
                        //Collides with bottom of player.
                        return true;
                    }
                }
            }

            return false;
        }

        public override void Update(long elapsedTime)
        {
            CheckWeather();

            if (changingToSteam)
            {
                changeCounter += elapsedTime;

                drawRectangle = new Rectangle?(new Rectangle(0, (int)(64 - (64 * (changeCounter / 4000.0f))), 64, 64));
                
                if (changeCounter >= 4000)
                {
                    weather = GameTeam.Weather.Hot;
                    changingToSteam = false;
                    changeCounter = 0;

                }
            }
            else if (changingFromSteam)
            {
                changeCounter += elapsedTime;

                drawRectangle = new Rectangle?(new Rectangle(0, (int)((64 * (changeCounter / 4000.0f))), 64, 64));

                if (changeCounter >= 4000)
                {
                    weather = GameTeam.Weather.Warm;
                    changingFromSteam = false;
                    changeCounter = 0;

                }
            }
            else if (changingToIce)
            {
                changeCounter += elapsedTime;

                icePercentage = (int)(255 * (changeCounter / 4000.0f));

                if (changeCounter >= 4000)
                {
                    weather = GameTeam.Weather.Cold;
                    drawRectangle = new Rectangle?(new Rectangle(0, 128, 64, 64));
                    changingToIce = false;
                    changeCounter = 0;

                }
            }
            else if (changingFromIce)
            {
                changeCounter += elapsedTime;

                icePercentage = (int)(255 * (changeCounter / 4000.0f));

                if (changeCounter >= 4000)
                {
                    weather = GameTeam.Weather.Warm;
                    drawRectangle = new Rectangle?(new Rectangle(0, 64, 64, 64));
                    changingFromIce = false;
                    changeCounter = 0;

                }
            }
        }

        private void CheckWeather()
        {
            if (weather != gameTeam.nextWeather)
            {
                //weather = gameTeam.actualWeather;

                if (gameTeam.nextWeather == GameTeam.Weather.Hot)
                {
                    if (changingToSteam == false)
                    {
                        changeToSteam();    
                    }
                }
                else if (gameTeam.nextWeather == GameTeam.Weather.Warm)
                {
                    if (weather == GameTeam.Weather.Hot)
                    {
                        changeFromSteam();
                    }
                    else if (weather == GameTeam.Weather.Cold)
                    {
                        changeFromIce();
                    }
                }
                else
                {
                    if (changingToIce == false)
                    {
                        changeToIce();
                    }
                }
            }
        }

        private void changeFromIce()
        {
            drawRectangle = new Rectangle?(new Rectangle(0, 64, 64, 64));
            changingFromIce = true;
        }

        private void changeToIce()
        {
            drawRectangle = new Rectangle?(new Rectangle(0, 64, 64, 64));
            changingToIce = true;
        }

        private void changeToSteam()
        {
            changingToSteam = true;
        }


        private void changeFromSteam()
        {
            changingFromSteam = true;
        }

        public override void Draw(Vector2 position, SpriteBatch spriteBatch)
        {

            if (!changingToIce && !changingFromIce)
            {
                spriteBatch.Draw(texture, position, drawRectangle, Color.White);
            }
            else if (changingToIce)
            {
                Color color = new Color(255, 255, 255, icePercentage);

                spriteBatch.Draw(texture, position, iceRect, color);

                color = new Color(255, 255, 255, 255 - icePercentage);
                spriteBatch.Draw(texture, position, drawRectangle, color);

            }
            else if (changingFromIce)
            {
                Color color = new Color(255, 255, 255, 255-icePercentage);

                spriteBatch.Draw(texture, position, iceRect, color);

                color = new Color(255, 255, 255, icePercentage);
                spriteBatch.Draw(texture, position, drawRectangle, color);

            }
        }
        


    }
}
