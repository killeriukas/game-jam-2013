﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace HotAndCoolMDs
{
    public class MountainTile : Tile
    {

        public MountainTile(Texture2D texture, Vector2 size) :
            base(texture, size)
        {
            type = Type.MOUNTAIN;

        }
    }
}
