﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace HotAndCoolMDs
{
    public abstract class Tile
    {
        public enum Type
        {
            AIR,
            GRASS,
            GRASSEND,
            SAND,
            SANDEND,
            MOUNTAIN,
            MOUNTAINEND,
            ROAD,
            ROADEND,
            CITY,
            DOOR,
            WATER,
            FILL,
            COUNT
        }

        public const int STANDARD_SIZE = 64;

        protected Type type = Type.AIR;


        protected Vector2 size;
        protected Texture2D texture;

        public Tile()
        {
            //Don't use unless Draw is overrided to do nothing.
        }

        public Tile(Texture2D texture, Vector2 size)
        {
            this.texture = texture;
            this.size = size;
        }

        public virtual void Update(long elapsedTime)
        {
            //Do nothing
        }

        //tile position - TOP LEFT CORNER
        //newPlayerPosition - CENTER
        //playerSize - ?
        public virtual bool CheckCollisionWithPlayer(Vector2 tilePosition, Vector2 newPlayerPosition, Vector2 playerSize)
        {
            //Check collision
            if ((tilePosition.Y > newPlayerPosition.Y && tilePosition.Y + size.Y < newPlayerPosition.Y + playerSize.Y))
            {
                if (tilePosition.X < newPlayerPosition.X && tilePosition.X + size.X > newPlayerPosition.X)
                {
                    //Left of player
                    return true;

                }
                else if (tilePosition.X < newPlayerPosition.X + playerSize.X && tilePosition.X + size.X > newPlayerPosition.X + playerSize.X)
                {
                    //Right
                    return true;
                }
            }

            if (tilePosition.X < newPlayerPosition.X && tilePosition.X + size.X > newPlayerPosition.X)
            {
                if (tilePosition.Y < newPlayerPosition.Y && tilePosition.Y + size.Y > newPlayerPosition.Y)
                {
                    //Collides with top of player.
                    return true;

                }
                else if (tilePosition.Y < newPlayerPosition.Y + playerSize.Y && tilePosition.Y + size.Y > newPlayerPosition.Y + playerSize.Y)
                {
                    //Collides with bottom of player.
                    return true;
                }
            }
            else if (tilePosition.X < newPlayerPosition.X + playerSize.X && tilePosition.X + size.X > newPlayerPosition.X + playerSize.X)
            {
                if (tilePosition.Y < newPlayerPosition.Y && tilePosition.Y + size.Y > newPlayerPosition.Y)
                {
                    //Collides with top of player.
                    return true;
                }
                else if (tilePosition.Y < newPlayerPosition.Y + playerSize.Y && tilePosition.Y + size.Y > newPlayerPosition.Y + playerSize.Y)
                {
                    //Collides with bottom of player.
                    return true;
                }
            }

            return false;

        }

        public virtual void Draw(Vector2 position, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }

        public virtual void Draw(Vector2 position, SpriteBatch spriteBatch, bool mirror)
        {
            //spriteBatch.Draw(texture, position, Color.White);
            spriteBatch.Draw(texture, position, null, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.FlipHorizontally, 0.0f);
        }
    }
}
