﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HotAndCoolMDs
{
    public class DoorTile : Tile
    {

        public DoorTile(Texture2D texture, Vector2 size)
            : base(texture, size)
        {
            type = Type.DOOR;
        }


        public override bool CheckCollisionWithPlayer(Vector2 tilePosition, Vector2 newPlayerPosition, Vector2 playerSize)
        {

            //if you collision with door, you return true

            //need to return false in case collision didn't happen
            return false;
        }

        public override void Draw(Vector2 position, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }
    }
}
