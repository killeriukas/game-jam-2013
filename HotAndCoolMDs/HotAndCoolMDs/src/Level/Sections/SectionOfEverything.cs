﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace HotAndCoolMDs
{
    public class SectionOfEverything : LevelSection
    {
        public SectionOfEverything(Level map, Tile sectionTile, ContentManager content) :
            base(map, sectionTile, content)
        {
            backgrounds[0] = content.Load<Texture2D>("Level//Backgrounds//Sectors//Desert//background1_1");
            backgrounds[1] = content.Load<Texture2D>("Level//Backgrounds//Sectors//Desert//background1_2");
            backgrounds[2] = content.Load<Texture2D>("Level//Backgrounds//Sectors//Woods//background2_1");
            backgrounds[3] = content.Load<Texture2D>("Level//Backgrounds//Sectors//Woods//background2_2");
            backgrounds[4] = content.Load<Texture2D>("Level//Backgrounds//Sectors//Mountains//background3_1");
            backgrounds[5] = content.Load<Texture2D>("Level//Backgrounds//Sectors//Mountains//background3_2");
            backgrounds[6] = content.Load<Texture2D>("Level//Backgrounds//Sectors//Night//background4_1");
            backgrounds[7] = content.Load<Texture2D>("Level//Backgrounds//Sectors//Night//background4_2");
            backgrounds[8] = content.Load<Texture2D>("Level//Backgrounds//Sectors//City//background5_1");
            backgrounds[9] = content.Load<Texture2D>("Level//Backgrounds//Sectors//City//background5_2");
        }

        protected override void InitialiseLayout()
        {
            layout = new int[width, height];

            for (int i = 0; i < width; i++)
            {
                for (int c = 0; c < height; c++)
                {
                    if (c == height - 1)
                    {
                        layout[i, c] = (int)Tile.Type.GRASS;
                    }
                    //else if (c > height - 5)
                    //{
                    //    layout[i, c] = (int)Tile.Type.WATER;
                    //}
                    else
                    {
                        layout[i, c] = (int)Tile.Type.AIR;
                    }
                }
            }
            layout = MapCreator.createPath(layout, width, height, 5, height - 1);
        }

        public override Vector2 CheckCollision(Vector2 playerPosition, Vector2 playerSize)
        {
            return base.CheckCollision(playerPosition, playerSize);
        }

        public override void Update(GameTime gameTime)
        {

        }
    }
}
