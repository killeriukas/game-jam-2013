﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace HotAndCoolMDs
{
    public abstract class LevelSection
    {
        //Keep reference to owner.
        Level map;

        protected Texture2D[] backgrounds;
        protected Tile sectionTile;

        public int width = 553;
        public int height = 14;

        public Vector2 position = Vector2.Zero;

        public int[,] layout;// = new int[SECTION_WIDTH, SECTION_HEIGHT];

        public LevelSection(Level map, Tile sectionTile, ContentManager content)
        {
            backgrounds = new Texture2D[10];
            this.map = map;
            this.sectionTile = sectionTile;
            InitialiseLayout();
        }

        protected virtual void InitialiseLayout()
        {
            layout = new int[width, height];

            for (int i = 0; i < width; i++)
            {
                for (int c = 0; c < height; c++)
                {
                    layout[i, c] = (int)Tile.Type.AIR;
                }
            }
        }

        public abstract void Update(GameTime gameTime);

        public virtual Vector2 CheckCollision(Vector2 playerPosition, Vector2 playerSize)
        {

            int startCheckWidth = 0;
            int endCheckWidth = width;

            if (playerPosition.X < (width * 0.5f) + playerSize.X)
            {
                endCheckWidth = (int)(width * 0.5f);
            }
            else if (playerPosition.X > (width * 0.5f) - playerSize.X)
            {
                startCheckWidth = (int)(width * 0.5f);
            }
            else
            {
                startCheckWidth = (int)(width * 0.25f);
                endCheckWidth = (int)(width * 0.75f);
            }

            bool collided = false;
            for (int i = 0; i < width; i++)
            {
                for (int c = 0; c < height; c++)
                {
                    
                    if (layout[i, c] != (int)Tile.Type.AIR)
                    {
                        if (layout[i, c] == (int)Tile.Type.WATER)
                        {
                            collided = ((WaterTile)map.tileList[layout[i, c]]).CheckCollisionWithPlayer(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE), playerPosition, playerSize);
                        }
                        else
                        {
                            collided = map.tileList[layout[i, c]].CheckCollisionWithPlayer(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE), playerPosition, playerSize);
                        }
                        if (collided)
                        {
                            return new Vector2(i, c);
                        }
                    }
                }
            }
            return new Vector2(-1, -1);

        }

        public virtual void Draw(Camera camera, SpriteBatch spriteBatch)
        {
         
            Vector2 temporaryPosition = new Vector2(0, 0);
            spriteBatch.Draw(this.backgrounds[0], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 2048;
            spriteBatch.Draw(this.backgrounds[1], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 4096;
            spriteBatch.Draw(this.backgrounds[0], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 6144;
            spriteBatch.Draw(this.backgrounds[1], camera.TransformObjectPosition(temporaryPosition), Color.White);

            temporaryPosition.X = 8192;
            spriteBatch.Draw(this.backgrounds[2], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 10240;
            spriteBatch.Draw(this.backgrounds[3], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 12288;
            spriteBatch.Draw(this.backgrounds[2], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 14336;
            spriteBatch.Draw(this.backgrounds[3], camera.TransformObjectPosition(temporaryPosition), Color.White);

            temporaryPosition.X = 16384;
            spriteBatch.Draw(this.backgrounds[4], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 18432;
            spriteBatch.Draw(this.backgrounds[5], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 20480;
            spriteBatch.Draw(this.backgrounds[4], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 22528;
            spriteBatch.Draw(this.backgrounds[5], camera.TransformObjectPosition(temporaryPosition), Color.White);

            temporaryPosition.X = 24576;
            spriteBatch.Draw(this.backgrounds[6], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 26624;
            spriteBatch.Draw(this.backgrounds[7], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 28672;
            spriteBatch.Draw(this.backgrounds[6], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 30720;
            spriteBatch.Draw(this.backgrounds[7], camera.TransformObjectPosition(temporaryPosition), Color.White);
            
            temporaryPosition.X = 32768;
            spriteBatch.Draw(this.backgrounds[8], camera.TransformObjectPosition(temporaryPosition), Color.White);
            temporaryPosition.X = 34816;
            spriteBatch.Draw(this.backgrounds[9], camera.TransformObjectPosition(temporaryPosition), Color.White);


            for (int i = 0; i < width; i++)
            {
                for (int c = 0; c < height; c++)
                {
                    if (layout[i, c] == (int)Tile.Type.GRASS)
                    {
                        if ((i * Tile.STANDARD_SIZE) < 8192)
                        {
                            bool drawn = false;

                            if (i > 0)
                            {
                                if (layout[i - 1, c] == (int)Tile.Type.AIR)
                                {
                                    map.tileList[(int)Tile.Type.SANDEND].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch);
                                    drawn = true;
                                }
                            }

                            if (i < width - 1 && !drawn)
                            {
                                if (layout[i + 1, c] == (int)Tile.Type.AIR)
                                {
                                    map.tileList[(int)Tile.Type.SANDEND].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch, true);
                                    drawn = true;
                                }
                            }

                            if (!drawn) 
                            {
                                map.tileList[(int)Tile.Type.SAND].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch);
                            }
                        }
                        else if ((i * Tile.STANDARD_SIZE) < 16384)
                        {
                            bool drawn = false;

                            if (i > 0)
                            {
                                if (layout[i - 1, c] == (int)Tile.Type.AIR)
                                {
                                    map.tileList[(int)Tile.Type.GRASSEND].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch);
                                    drawn = true;
                                }
                            }

                            if (i < width - 1 && !drawn)
                            {
                                if (layout[i + 1, c] == (int)Tile.Type.AIR)
                                {
                                    map.tileList[(int)Tile.Type.GRASSEND].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch, true);
                                    drawn = true;
                                }
                            }

                            if (!drawn)
                            {
                                map.tileList[(int)Tile.Type.GRASS].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch);
                            }
                        }
                        else if ((i * Tile.STANDARD_SIZE) < 24576)
                        {
                            bool drawn = false;

                            if (i > 0)
                            {
                                if (layout[i - 1, c] == (int)Tile.Type.AIR)
                                {
                                    map.tileList[(int)Tile.Type.MOUNTAINEND].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch);
                                    drawn = true;
                                }
                            }

                            if (i < width - 1 && !drawn)
                            {
                                if (layout[i + 1, c] == (int)Tile.Type.AIR)
                                {
                                    map.tileList[(int)Tile.Type.MOUNTAINEND].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch, true);
                                    drawn = true;
                                }
                            }

                            if (!drawn)
                            {
                                map.tileList[(int)Tile.Type.MOUNTAIN].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch);
                            }
                        }
                        else if ((i * Tile.STANDARD_SIZE) < 34432)
                        {
                            bool drawn = false;

                            if (i > 0)
                            {
                                if (layout[i - 1, c] == (int)Tile.Type.AIR)
                                {
                                    map.tileList[(int)Tile.Type.ROADEND].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch);
                                    drawn = true;
                                }
                            }

                            if (i < width - 1 && !drawn)
                            {
                                if (layout[i + 1, c] == (int)Tile.Type.AIR)
                                {
                                    map.tileList[(int)Tile.Type.ROADEND].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch, true);
                                    drawn = true;
                                }
                            }

                            if (!drawn)
                            {
                                map.tileList[(int)Tile.Type.ROAD].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch);
                            }
                        }
                        else
                        {
                            map.tileList[(int)Tile.Type.CITY].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch);
                        }
                    }
                    else
                    {
                        map.tileList[layout[i, c]].Draw(camera.TransformObjectPosition(new Vector2(position.X + (i * Tile.STANDARD_SIZE), c * Tile.STANDARD_SIZE)), spriteBatch);
                    }
                }
            }
        }
    }
}
