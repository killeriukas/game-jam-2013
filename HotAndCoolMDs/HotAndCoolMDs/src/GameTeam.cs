﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using GardeGames.Graphics2D;
using HotAndCoolMDs.src;

namespace HotAndCoolMDs
{
    public class GameTeam
    {
        internal enum Weather
        {
            Cold,
            Warm,
            Hot
        };

        private const float GRAVITY = 0.978f; // directly affects acceleration, like gravity

        internal Weather actualWeather, nextWeather;
        int totalChangeTime, actualChangeTime;
        float timer;

        public PlayerIndex playerIndex1, playerIndex2;
        Dictionary<PlayerIndex, GamePadState> prevState;
        Dictionary<Weather, Effect> weatherEffects;
        RenderTarget2D targetBefore, targetFinal;
        GraphicsDevice device;
        Player player1, player2;

        //For side scrolling
        internal Camera camera;
        internal Level level;

        Transportable[] objects;
        Transportable heart;

        bool finished;

        int helperTimer;

        Animation[] m_animations;

        public GameTeam(Animation[] animations, Effect effect1, Effect effect2, Effect effectCool, Effect effectHot, PlayerIndex playerIndex1, PlayerIndex playerIndex2, Vector2 screenSize, GraphicsDevice device)
        {
            this.playerIndex1 = playerIndex1;
            this.playerIndex2 = playerIndex2;

            Animation[] playerAnimations1 = new Animation[3];

            playerAnimations1[0] = animations[0];
            playerAnimations1[1] = animations[1];
            playerAnimations1[2] = animations[0];

            Animation[] playerAnimations2 = new Animation[3];

            playerAnimations2[0] = animations[2];
            playerAnimations2[1] = animations[3];
            playerAnimations2[2] = animations[2];

            player1 = new Player(playerAnimations1, effect1, new Vector2(50, 10), Player.Ability.Heat);
            player2 = new Player(playerAnimations2, effect2, new Vector2(50, 100), Player.Ability.Cool);

            prevState = new Dictionary<PlayerIndex, GamePadState>();
            prevState.Add(playerIndex1, GamePad.GetState(playerIndex1));
            prevState.Add(playerIndex2, GamePad.GetState(playerIndex2));

            weatherEffects = new Dictionary<Weather, Effect>();
            weatherEffects.Add(Weather.Cold, effectCool);
            //set animations for later use
            m_animations = animations;
            weatherEffects.Add(Weather.Hot, effectHot);

            //2560, 768
            targetBefore = new RenderTarget2D(device, 3000, 900);//(int)screenSize.X * 2, (int)screenSize.Y);
            targetFinal = new RenderTarget2D(device, 3000, 900);
            this.device = device;

            actualWeather = nextWeather = Weather.Warm;

            totalChangeTime = actualChangeTime = 4000; // milliseconds
            //Load and Set map

            camera = new Camera();

            timer = 0;

            helperTimer = 3000;
            finished = false;
        }

        public void startLevel(Level level)
        {
            this.level = level;


            objects = new Transportable[300];
            objects[0] = new Transportable(Transportable.Type.Heart, m_animations[4], new Vector2(3 * Tile.STANDARD_SIZE, (level.currentSection.height - 5) * Tile.STANDARD_SIZE), -1);
            heart = objects[0];

            int counter = 1;

            //go through all the array of current scene
            for (int i = 0; i < level.currentSection.width; ++i)
            {
                for (int j = 0; j < level.currentSection.height; ++j)
                {

                    //if doors are found, set the key for them
                    if (level.currentSection.layout[i, j] == (int)Tile.Type.DOOR)
                    {
                        //set the new transposible key with key animation into position of vector and id of j variable, which is X axis
                        objects[counter++] = createNewKey(i, j);
                    }
                }
            }

        }
        Random rand = new Random();
        private Transportable createNewKey(int x, int y)
        {
            Vector2 position = new Vector2(0, 0);
            position.X = rand.Next(x - 35, x);

            if (position.X < 0)
            {
                position.X = 4;
            }

            for (int i = 0; i < level.currentSection.height; i++)
            {
                if (level.currentSection.layout[(int)position.X, i] == (int)Tile.Type.GRASS ||
                    level.currentSection.layout[(int)position.X, i] == (int)Tile.Type.FILL)
                {
                    position.Y = i - 1;
                    break;
                }
            }
            position.X *= Tile.STANDARD_SIZE;
            position.Y *= Tile.STANDARD_SIZE;

            return new Transportable(Transportable.Type.Key, m_animations[5], position, x);

        }

        public void update(long elapsedTime)
        {
            if (finished)
            {
                helperTimer -= (int)elapsedTime;
                return;
            }

            level.Update(elapsedTime);
            updatePlayer(elapsedTime, playerIndex1, player1);
            updatePlayer(elapsedTime, playerIndex2, player2);
            

            timer += elapsedTime;
            if (timer > 1000)
            {
                timer -= 1000;
            }

            for (int i = 0; i < objects.Length; i++)
            {
                if (objects[i] != null)
                {
                    objects[i].update(elapsedTime);
                }
                else
                {
                    break;
                }
                
            }  

            if (actualChangeTime < totalChangeTime)
            {
                actualChangeTime += (int)elapsedTime;
                if (actualChangeTime >= totalChangeTime)
                {
                    actualWeather = nextWeather;
                }
            }
        }

        private void updatePlayer(long elapsedTime, PlayerIndex playerIndex, Player player)
        {
            if (player.getPosition().X < 0.0f)
            {
                Vector2 pos = player.getPosition();
                pos.X = 0.0f;
                player.setPosition(pos);
            }
            else if (player.getPosition().X > (level.currentSection.width - 1) * Tile.STANDARD_SIZE)
            {
                Vector2 pos = player.getPosition();
                pos.X = (level.currentSection.width - 1) * Tile.STANDARD_SIZE;
                player.setPosition(pos);
            }

            player.update(elapsedTime);
            //apply gravity
            player.setVertSpeed(player.getSpeed().Y + GRAVITY*0.025f);

            Vector2 stick = Vector2.UnitY;
            if (GamePad.GetState(playerIndex).IsConnected)
            {
                stick = GamePad.GetState(playerIndex).ThumbSticks.Left;
                stick.Y = 0;
            }
            else
            {
                if (Keyboard.GetState().IsKeyDown(Keys.A))
                {
                    stick.X--;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.D))
                {
                    stick.X++;
                }
            }
            stick *= elapsedTime * player.getSpeed();

            stick.Y = 0;

            // collision with X
            Vector2 oldPosition = player.getPosition();
            Vector2 newPosition = oldPosition + stick;
            //Vector2 tilePos = level.CheckCollision(newPosition, new Vector2(player.getFrameTexture().Width * 0.5f, player.getFrameTexture().Height * 0.5f));
            Vector2 tilePos = level.getTileCollision(player.getPosition(), player.getSize(), newPosition.X, oldPosition.Y, false);

            bool aligning = true;

            if (tilePos.X == -1 && tilePos.Y == -1)
            {
                player.setPosition(newPosition);
            }
            else
            {
                bool align = true;
                //check whether collision was with the doors
                if (level.currentSection.layout[(int)tilePos.X, (int)tilePos.Y] == (int)Tile.Type.DOOR)
                {

                    //CHECK WHETHER THE PLAYER HAS A KEY
                    //check all the objects whether any of them is on the doors
                    for (int i = 0; i < objects.Length; ++i)
                    {
                        if (objects[i] != null)
                        {
                            //if this is a Key
                            //because Key returns ID, not -1
                            int keyID = objects[i].GetKeyID();

                            int keyLeft = (int)(objects[i].getPosition().X / 64) + 1;
                            int keyRight = (int)(objects[i].getPosition().X / 64) - 1;

                            if (keyID != -1 && keyID == (int)tilePos.X && (keyLeft == (int)tilePos.X || keyRight == (int)tilePos.X))
                            {
                                //IF YES, THEN DELETE THE DOORS AND A KEY AND NOT COLLIDE WITH PLAYER
                                //deletes the doors
                                //changes the doors tile with air tile
                                level.currentSection.layout[(int)tilePos.X, (int)tilePos.Y] = 0;

                                align = false;
                            }
                            else
                            {
                                align = true;
                            }

                            //if (key != -1 && key != (int)tilePos.X)
                            //{
                            //    alignHorizontal(stick.X, player, tilePos.X, tilePos.X + 1);
                            //}
                        }
                        else
                        {
                            break;
                        }

                    }

                    //IF YES, THEN DELETE THE DOORS AND A KEY AND NOT COLLIDE WITH PLAYER

                    //IF NO, THEN COLLIDE WITH PLAYER AND DO NOTHING

                }
                if (align && aligning) { aligning = false; alignHorizontal(stick.X, player, tilePos.X, tilePos.X + 1); }
             
               
            }

            // collision with Y
            newPosition.Y = oldPosition.Y + (float)(GRAVITY * player.getSpeed().Y * elapsedTime);
            tilePos = level.getTileCollision(player.getPosition(), player.getSize(), oldPosition.X, newPosition.Y, true);
            if (tilePos.X == -1 && tilePos.Y == -1)
            {
                player.setPosition(newPosition);

                //no collision
                player.setCanJump(false);
            }
            else
            {                
                if (aligning) { alignVertical(player.getSpeed().Y, player, tilePos.Y, tilePos.Y + 1); }
            }

            if (player.getPosition().Y > targetBefore.Height - 66)
            {
                player.setCanJump(true);
            }

            //Move camera
            if (stick.X > 0.0f) // Move right
            {
                if (camera.TransformObjectPosition(player.getPosition()).X >= (targetBefore.Width * 0.5f))
                {
                    camera.position.X += stick.X;
                }

            }
            else if (stick.X < 0.0f) // Move left
            {
                if (camera.TransformObjectPosition(player.getPosition()).X <= (targetBefore.Width * 0.2f))
                {
                    if (camera.position.X + stick.X > 0.0f)
                    {
                        camera.position.X += stick.X;
                    }
                }
            }

            if (GamePad.GetState(playerIndex).Buttons.B == ButtonState.Pressed && actualChangeTime >= totalChangeTime && !player.grabbing)
            {
                Player.Ability ability = player.changeWeather();
                if (ability == Player.Ability.Heat)
                {
                    if (actualWeather != Weather.Hot)
                    {
                        nextWeather = actualWeather + 1;
                        actualChangeTime = 0;
                    }
                }
                else//cool
                {
                    if (actualWeather != Weather.Cold)
                    {
                        nextWeather = actualWeather - 1;
                        actualChangeTime = 0;
                    }
                }
            }
            if (!GamePad.GetState(playerIndex).IsConnected && Keyboard.GetState().IsKeyDown(Keys.Q) && actualChangeTime >= totalChangeTime && !player.grabbing)
            {
                if (actualWeather != Weather.Hot)
                {
                    nextWeather = actualWeather + 1;
                    actualChangeTime = 0;
                }
            }
            if (!GamePad.GetState(playerIndex).IsConnected && Keyboard.GetState().IsKeyDown(Keys.E) && actualChangeTime >= totalChangeTime && !player.grabbing)
            {
                if (actualWeather != Weather.Cold)
                {
                    nextWeather = actualWeather - 1;
                    actualChangeTime = 0;
                }
            }

            if (GamePad.GetState(playerIndex).Buttons.X == ButtonState.Pressed && prevState[playerIndex].Buttons.X == ButtonState.Released)
            {
                Transportable t = grabClosestObject(player);
                if (!player.grabbing)
                {
                    if (t != null && !t.isAttached())
                    {
                        player.grabbing = true;
                        t.grab(player);
                    }
                }
                else
                {
                    player.grabbing = false;
                    t.drop();
                }
            }
            if (!GamePad.GetState(playerIndex).IsConnected && Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                Transportable t = grabClosestObject(player);
                if (!player.grabbing)
                {
                    if (t != null && !t.isAttached())
                    {
                        player.grabbing = true;
                        t.grab(player);
                    }
                }
                else
                {
                    player.grabbing = false;
                    t.drop();
                }
            }

            if (GamePad.GetState(playerIndex).Buttons.A == ButtonState.Pressed)
            {
                player.jump(0.9f);
            }
            if (!GamePad.GetState(playerIndex).IsConnected && Keyboard.GetState().IsKeyDown(Keys.W))
            {
                player.jump(0.9f);
            }

            //remember last pad state
            prevState[playerIndex] = GamePad.GetState(playerIndex);

            //checks the distance between the goals
            CheckFinishOfGame();
           

            //change player state
            if (actualWeather != nextWeather) //casting
            {
                if (actualWeather < nextWeather)
                {
                    if (player.changeWeather() == Player.Ability.Heat)
                    {
                        player.setState(Player.States.SpellCast);
                    }
                }
                else
                {
                    if (player.changeWeather() == Player.Ability.Cool)
                    {
                        player.setState(Player.States.SpellCast);
                    }
                }
            }
            else if (player.getSpeed().Y != 0) //jump
            {
                player.setState(Player.States.Stop);
            }
            else
            {
                if (stick.X != 0)
                {
                    player.setState(Player.States.Walk);
                }
                else
                {
                    player.setState(Player.States.Stop);
                }
            }
            if (stick.X != 0)
            {
                player.facesRight = stick.X > 0;
            }

        }

        private void CheckFinishOfGame()
        {
            //go through the array to find a heart
            for (int i = 0; i < objects.Length; ++i)
            {

                if (objects[i] != null)
                {
                    //if this is not herat ID, skip the rest of the function
                    if (objects[i].GetKeyID() != -1)
                    {
                        continue;
                    }

                    //take x position of the heart
                    float keyPosition = objects[i].getPosition().X;

                    //check whether heart moved through the patient
                    if (keyPosition > 35328.0f)
                    {
                        //if yes, finish the game
                        finished = true;
                    }
                }
                else
                {
                    break;
                }
            }
        }

        private void alignVertical(float speed, Player player, float tileUp, float tileDown)
        {
            tileUp *= 64;
            tileDown *= 64;
            Vector2 position = player.getPosition();
            if (speed > 0)
            {
                position.Y = (int)(tileUp - player.getSize().Y * 0.5f);
                //touches ground
                player.setCanJump(true);
                player.setVertSpeed(0);
            }
             if (speed < 0)
            {
                position.Y = (int)(tileDown + player.getSize().Y * 0.5f);
                //touches top with head
                player.setVertSpeed(0);
            }
            player.setPosition(position);
        }

        private void alignHorizontal(float speed, Player player, float tileLeft, float tileRight)
        {
            tileLeft *= 64;
            tileRight *= 64;
            Vector2 position = player.getPosition();
            if (speed > 0)
            {
                position.X = (int)(tileLeft - player.getSize().X * 0.5f);
            }
            else if (speed < 0)
            {
                position.X = (int)(tileRight + player.getSize().X * 0.5f);
            }
            player.setPosition(position);
        }


        private Transportable grabClosestObject(Player player)
        {
            Transportable t = null;
            float distance = 50*50; // it has to be near
            for (int i = 0; i < objects.Length; i++)
            {
                if (objects[i] != null)
                {
                    float d = (objects[i].getPosition() - player.getPosition()).LengthSquared();
                    if (d == 0)
                    {
                        return objects[i];//no need to compare with others
                    }
                    if (distance > d)
                    {
                        distance = d;
                        t = objects[i];
                    }
                }
                else 
                {
                    break;
                }

            }
            return t;
        }


        public void draw(SpriteBatch s)
        {
            device.SetRenderTarget(targetBefore);
            device.Clear(Color.White);

            //draw doctors
            s.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            //draw map
            level.Draw(camera, s);

            drawPlayer(s, player1);
            drawPlayer(s, player2);
            s.End();
            //draw rest
            s.Begin();
            for (int i = 0; i < objects.Length; i++)
            {
                drawTransportable(s, objects[i]);
            }  
            s.End();

            if (actualWeather != Weather.Warm)
            {
                device.SetRenderTarget(targetFinal);
                device.Clear(Color.Black);
                s.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

                weatherEffects[actualWeather].Parameters["timer"].SetValue(timer/1000);
                weatherEffects[actualWeather].CurrentTechnique.Passes[0].Apply();
                s.Draw(targetBefore, Vector2.Zero, Color.White);

                s.End();
            }

            device.SetRenderTarget(null);
            device.Clear(Color.Black);
        }


        private void drawPlayer(SpriteBatch s, Player player)
        {
            player.applyInkEffect();
            KeyValuePair<Texture2D, Rectangle> structure = player.getSpriteSheet();
            Vector2 v = new Vector2(structure.Value.Width / 2, structure.Value.Height / 2);
            SpriteEffects sprtFx = !player.facesRight ? SpriteEffects.FlipHorizontally : SpriteEffects.None;
            s.Draw(structure.Key, camera.TransformObjectPosition(player.getPosition()), structure.Value, Color.White, 0, v, 1.0f, sprtFx, 0);
        }


        private void drawTransportable(SpriteBatch s, Transportable trans)
        {
            if (trans != null)
            {
                Vector2 v = new Vector2(trans.getFrameTexture().Width / 2, trans.getFrameTexture().Height / 2);
                s.Draw(trans.getFrameTexture(), camera.TransformObjectPosition(trans.getPosition()), null, Color.White, 0, v, 0.5f, SpriteEffects.None, 0);
            }
        }


        public RenderTarget2D getRenderTarget()
        {
            if (actualWeather != Weather.Warm)
                return targetFinal;
            else
                return targetBefore;
        }


        public bool isFinished()
        {
            return finished && helperTimer <= 0;
        }

        public bool isFinishing()
        {
            return helperTimer < 3000;
        }

        internal bool holdingHeart()
        {
            return heart.isAttached();
        }

        internal PlayerIndex getHolderIndex()
        {
            return heart.grabber == player1 ? playerIndex1 : playerIndex2;
        }
    }
}
