﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GardeGames.Input
{
    /**
     * controla los eventos de teclado,
     * actualizando las Acciones de Juego correspondientes
     */
    public class InputManager
    {
        // mouse codes
        public const int MOUSE_MOVE_LEFT = 0;
        public const int MOUSE_MOVE_RIGHT = 1;
        public const int MOUSE_MOVE_UP = 2;
        public const int MOUSE_MOVE_DOWN = 3;
        public const int MOUSE_WHEEL_UP = 4;
        public const int MOUSE_WHEEL_DOWN = 5;
        public const int MOUSE_BUTTON_1 = 6;
        public const int MOUSE_BUTTON_2 = 7;
        public const int MOUSE_BUTTON_3 = 8;

        public const int NUM_MOUSE_CODES = 9;

        private const int NUM_TECLAS = 600;
        private GameAction[] accionesTeclas;
        private GameAction[] accionesRaton;
        private Vector2 posRaton;
        private Vector2 posCentro;

        private MouseState ratonActual, ratonAnterior;
        private bool ratonRelativo;

        public InputManager(GraphicsDeviceManager graphics)
        {
            accionesTeclas = new GameAction[NUM_TECLAS];
            accionesRaton = new GameAction[NUM_MOUSE_CODES];
            ratonRelativo = false;
            posCentro = new Vector2(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2);
        }

        public void setModoRatonRelativo(bool relativo)
        {
            ratonRelativo = relativo;
        }

        public bool getModoRatonRelativo()
        {
            return ratonRelativo;
        }

        public void mapearTeclado(GameAction accion, Keys tecla)
        {
            accionesTeclas[(int)tecla] = accion;
        }

        public void mapearRaton(GameAction accion, int codigo)
        {
            accionesRaton[codigo] = accion;
        }

        public float getMouseX()
        {
            return posRaton.X;
        }

        public float getMouseY()
        {
            return posRaton.Y;
        }

        public Vector2 getDesplazamientoRaton()
        {
            return new Vector2(Mouse.GetState().X - posRaton.X, Mouse.GetState().Y - posRaton.Y);
        }

        public void centrarRaton()
        {
            moverRaton(posCentro);
        }

        public void moverRaton(Vector2 posicion)
        {
            Mouse.SetPosition((int)posicion.X, (int)posicion.Y);
        }

        public void actualizarTeclado()
        {
            // teclas
            for (int i = 0; i < NUM_TECLAS; i++)
            {
                if (accionesTeclas[i]!=null && Keyboard.GetState().IsKeyDown((Keys)i))
                {
                    accionesTeclas[i].press();
                }
                else if (accionesTeclas[i] != null && Keyboard.GetState().IsKeyUp((Keys)i))
                {
                    accionesTeclas[i].release();
                }
            }

            //Raton
            int dx = (int)(Mouse.GetState().X - posRaton.X);
            int dy = (int)(Mouse.GetState().Y - posRaton.Y);
            mouseHelper(MOUSE_MOVE_LEFT, MOUSE_MOVE_RIGHT, dx);
            mouseHelper(MOUSE_MOVE_UP, MOUSE_MOVE_DOWN, dy);
            mouseHelper(MOUSE_WHEEL_UP, MOUSE_WHEEL_DOWN, Mouse.GetState().ScrollWheelValue);

            MouseState estado = Mouse.GetState();
            //boton izquierdo
            if (accionesRaton[MOUSE_BUTTON_1]!=null && estado.LeftButton == ButtonState.Pressed)
            {
                accionesRaton[MOUSE_BUTTON_1].press();
            }
            else if (accionesRaton[MOUSE_BUTTON_1] != null && estado.LeftButton == ButtonState.Released)
            {
                accionesRaton[MOUSE_BUTTON_1].release();
            }
            //boton derecho
            if (accionesRaton[MOUSE_BUTTON_2] != null && estado.RightButton == ButtonState.Pressed)
            {
                accionesRaton[MOUSE_BUTTON_2].press();
            }
            else if (accionesRaton[MOUSE_BUTTON_2] != null && estado.RightButton == ButtonState.Released)
            {
                accionesRaton[MOUSE_BUTTON_2].release();
            }
            //boton central
            if (accionesRaton[MOUSE_BUTTON_3] != null && estado.MiddleButton == ButtonState.Pressed)
            {
                accionesRaton[MOUSE_BUTTON_3].press();
            }

            else if (accionesRaton[MOUSE_BUTTON_3] != null && estado.MiddleButton == ButtonState.Released)
            {
                accionesRaton[MOUSE_BUTTON_3].release();
            }

            posRaton.X = Mouse.GetState().X;
            posRaton.Y = Mouse.GetState().Y;

            if (ratonRelativo)
            {
                centrarRaton();
            }
        }

        private void mouseHelper(int codeNeg, int codePos,
            int amount)
        {
            GameAction gameAction;
            if (amount < 0)
            {
                gameAction = accionesRaton[codeNeg];
            }
            else
            {
                gameAction = accionesRaton[codePos];
            }
            if (gameAction != null)
            {
                gameAction.press(Math.Abs(amount));
                gameAction.release();
            }
        }

    }

}
