﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GardeGames.Graphics2D
{
    public class Animation
    {

        List<AnimFrame> frames;
        private int currFrameIndex;
        private long animTime;
        private long totalDuration;

        //for spriteSheet version
        Texture2D spriteSheet;
        int numFrames;

        /**
        Creates a new, empty Animacion.
        */
        public Animation() {
            frames=new List<AnimFrame>();
            spriteSheet = null;
            totalDuration = 0;
        }

        public Animation(List<AnimFrame> frames, long duracion) {
            this.frames=frames;
            totalDuration=duracion;
        }

        /**
        Creates a new spriteSheet Animacion.
        */
        public Animation(Texture2D spriteSheet, int numFrames, int totalDuration)
        {
            frames = null;
            this.spriteSheet = spriteSheet;
            this.numFrames = numFrames;
            this.totalDuration = totalDuration;
        }


        /**
            Creates a duplicate of this Animacion. The list of frames
            are shared between the two Animacions, but each Animacion
            can be animated independently.
        */
        public Animation clone() {
            return new Animation(frames, totalDuration);
        }


        /**
            Adds an image to the Animacion with the specified
            duration (time to display the image).
        */
        public void addFrame(Texture2D imagen,
            long duration)
        {
            totalDuration += duration;
            frames.Add(new AnimFrame(imagen, totalDuration));
        }


        /**
            Starts this Animacion over from the beginning.
        */
        public void start() {
            animTime = 0;
            currFrameIndex = 0;
        }


        /**
            Updates this Animacion's current image (frame), if
            neccesary.
        */
        public void update(long elapsedTime)
        {
            if (spriteSheet != null)
            {
                animTime += elapsedTime;

                if (animTime >= totalDuration)
                {
                    animTime = animTime % totalDuration;
                }
                currFrameIndex = (int)((numFrames * animTime) / totalDuration);


            }
            else if (frames.Count > 1)
            {
                animTime += elapsedTime;

                if (animTime >= totalDuration)
                {
                    animTime = animTime % totalDuration;
                    currFrameIndex = 0;
                }

                while (animTime > getFrame(currFrameIndex).endTime)
                {
                    currFrameIndex++;
                }
            }
        }


        /**
            Gets this Animacion's current image. Returns null if this
            Animacion has no images.
        */
        public Texture2D getTexture2D() {
            if (spriteSheet != null)
            {
                return spriteSheet;
            }
            if (frames.Count == 0) {
                return null;
            }
            else {
                return getFrame(currFrameIndex).imagen;
            }
        }

        public KeyValuePair<Texture2D, Rectangle> getSpriteSheetFrame()
        {
            Rectangle r = new Rectangle();
            r.X = (spriteSheet.Width / numFrames) * currFrameIndex;
            r.Y = 0;
            r.Width = spriteSheet.Width / numFrames;
            r.Height = spriteSheet.Height;
            return new KeyValuePair<Texture2D, Rectangle>(spriteSheet, r);

        }


        private AnimFrame getFrame(int i) {
            return frames.ElementAt<AnimFrame>(i);
        }


        public class AnimFrame {

            public Texture2D imagen;
            public long endTime;

            public AnimFrame(Texture2D imagen, long endTime) {
                this.imagen = imagen;
                this.endTime = endTime;
            }
        }


    }
}
