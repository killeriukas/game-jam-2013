﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using GardeGames.States;
using GardeGames.Input;

namespace GardeGames.Core
{
    /* 
     * Esta es la clase principal, 
     * de la que se debe extender para crear el nucleo del juego
     */
    public abstract class MainGame : Microsoft.Xna.Framework.Game
    {

        //Graficos2D
        GraphicsDeviceManager graphics;

        //dibujo
        SpriteBatch spriteBatch;

        //estados
        protected StateManager stateManager;

        //entrada
        protected InputManager managerTeclado;
        
        //recursos
        protected ResourceManager cargadorRecursos;

        //singleton
        MainGame game;

        /*
         * crea un juego a pantalla completa
         */
        public MainGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            cargadorRecursos = new ResourceManager(Content);
            
            //fullscreen
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.IsFullScreen = true;

            managerTeclado = new InputManager(graphics);

            stateManager = new StateManager(null, managerTeclado);

            game = this;
        }

        /*
         * crea un juego en una ventana de tamaño concreto
         */
        public MainGame(int width, int height)
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            cargadorRecursos = new ResourceManager(Content);


            //windowed
            graphics.PreferredBackBufferWidth = width;
            graphics.PreferredBackBufferHeight = height;
            graphics.IsFullScreen = false;
            managerTeclado = new InputManager(graphics);

            stateManager = new StateManager(null, managerTeclado);

            game = this;
        }

        public MainGame getGame()
        {
            return game;
        }

        /*
         * Inicializa las variables necesarias
         */
        protected override void Initialize()
        {
            //hacemos el bucle contante, cada 10 ms
            IsFixedTimeStep = true;
            TargetElapsedTime = new TimeSpan(0, 0, 0, 0, 10);

            base.Initialize();
        }

        /*
         * Carga los contenidos
         */
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            stateManager.loadAllResources(cargadorRecursos, GraphicsDevice);
        }

        /*
         * Actualiza el juego, permitiendo salir del mismo
         */
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                //this.Exit();
            if (stateManager.isDone())
                this.Exit();

            // TODO: Add your update logic here

            long elapsedTime = gameTime.ElapsedGameTime.Milliseconds;

            stateManager.update(elapsedTime);
            managerTeclado.actualizarTeclado();

            base.Update(gameTime);
        }

        /*
         * Dibuja en pantalla
         */
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            //spriteBatch.Begin();

            stateManager.draw(spriteBatch);

            //spriteBatch.End();

            base.Draw(gameTime);
        }

    }
}
