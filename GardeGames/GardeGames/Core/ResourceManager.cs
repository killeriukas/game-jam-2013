﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Drawing;

namespace GardeGames.Core
{
    public class ResourceManager
    {
        public ContentManager content;
        GraphicsDevice Graficos2D;

        public ResourceManager(ContentManager content)
        {
            this.content = content;
            Graficos2D = null;
        }

        public ResourceManager(GraphicsDevice Graficos2D)
        {
            this.Graficos2D = Graficos2D;
            content = null;
        }

        public Texture2D loadTexture2D(String image)
        {
            if (content != null)
            {
                return content.Load<Texture2D>(image);
            }
            else
            {
                return null;// Texture2D.FromStream(Graficos2D, imagen);
            }
        }

        public Bitmap change2bitmap(Texture2D texture)
        {
            byte[] textureData = new byte[4 * texture.Width * texture.Height];
            texture.GetData<byte>(textureData);

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(
                           texture.Width, texture.Height,
                           System.Drawing.Imaging.PixelFormat.Format32bppArgb
                         );

            System.Drawing.Imaging.BitmapData bmpData = bmp.LockBits(
                           new System.Drawing.Rectangle(0, 0, texture.Width, texture.Height),
                           System.Drawing.Imaging.ImageLockMode.WriteOnly,
                           System.Drawing.Imaging.PixelFormat.Format32bppArgb
                         );

            IntPtr safePtr = bmpData.Scan0;
            System.Runtime.InteropServices.Marshal.Copy(textureData, 0, safePtr, textureData.Length);
            bmp.UnlockBits(bmpData);
            return bmp;
        }

        public SpriteFont loadSpriteFont(String font)
        {
            return content.Load<SpriteFont>(font);
        }

        public Model loadModel(String model)
        {
            return content.Load<Model>(model);
        }

    }
}
