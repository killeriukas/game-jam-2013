﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using GardeGames.Input;
using GardeGames.Core;

namespace GardeGames.States
{
    public class StateManager
    {
        public const string END_GAME = "salir juego";

        List<State> estados;
        List<String> nombreEstados;
        private Texture2D imagenInicial;
        private State estadoActual;
        private bool hecho;
        private InputManager managerTeclado;

        public StateManager(Texture2D imagenInicial, InputManager managerTeclado)
        {
            this.imagenInicial = imagenInicial;
            estados = new List<State>();
            nombreEstados = new List<String>();
            this.managerTeclado = managerTeclado;
        }

        public void addState(State state) {
            estados.Add(state);
            nombreEstados.Add(state.getName());
        }

        public List<State> getStates() {
            return estados.ToList<State>();
        }

        public void loadAllResources(ResourceManager recursos, GraphicsDevice Graficos2D)
        {
            List<State> lista = getStates();
            State e;
            for (int i = 0; i < lista.Count;i++)
            {
                e = lista.ElementAt<State>(i);
                e.loadResources(recursos,Graficos2D);
            }
        }


        public bool isDone() {
            return hecho;
        }


        /**
            Sets the current state (by name).
        */
        public void setState(String name) {
            // clean up old state
            if (estadoActual != null) {
                estadoActual.stop();
            }

            if (name.Equals(END_GAME))
            {
                hecho = true;
            }
            else {

                // set new state
                estadoActual = (State)getEstado(name);
                if (estadoActual != null) {
                    estadoActual.start(managerTeclado);
                }
            }
        }

        private State getEstado(string name)
        {
            int i = nombreEstados.IndexOf(name);
            return estados.ElementAt<State>(i);
        }


        /**
            Updates world, handles input.
        */
        public void update(long elapsedTime) {
            String nextState = estadoActual.checkForStateChange();
            if (nextState != null) {
                setState(nextState);
            }
            else {
                estadoActual.update(elapsedTime);
            }
        }


        /**
            Dibuja en la pantalla con un SpriteBatch ya abierto
        */
        public void draw(SpriteBatch s) {
            if (estadoActual != null) {
                estadoActual.draw(s);
            }
            else {
                // if no state, draw the default image to the screen
                s.Draw(imagenInicial,new Rectangle(0,0,100,100),Color.White);
            }
        }
    }
}
