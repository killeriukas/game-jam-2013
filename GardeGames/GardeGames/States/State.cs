﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using GardeGames.Input;
using GardeGames.Core;

namespace GardeGames.States
{
    public abstract class State
    {

        string name;
        string nextState;

        public State(string name)
        {
            this.name = name;
            nextState = null;
        }

        /**
        Gets the name of this state. Used for
        the checkForStateChange() method.
        */
        public String getName()
        {
            return name;
        }

        public void setNextState(string state)
        {
            nextState = state;
        }

        /**
            Returns the name of a state to change to if this state is
            ready to change to another state, or null otherwise.
        */
        public String checkForStateChange()
        {
            String next = nextState;
            nextState = null;
            return next;
        }


        /**
            Loads any resources for this state. This method is called
            in a background thread before any GameStates are set.
        */
        public abstract void loadResources(ResourceManager recursos, GraphicsDevice Graficos2D);

        /**
            Initializes this state and sets up the input manager
        */
        public abstract void start(InputManager managerTeclado);


        /**
            Performs any actions needed to stop this state.
        */
        public abstract void stop();


        /**
            Updates world, handles input.
        */
        public abstract void update(long elapsedTime);


        /**
            Draws to the screen.
        */
        public abstract void draw(SpriteBatch s);
    }
}
